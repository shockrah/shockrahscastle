+++
title = "Projects"
weight = 30
draft = false
+++

I'm listing my _top 3_ personal favorite projects to work on right now but all of my projects can be found [here](https://gitlab.com/shockrahwow/projects)

## [ESP MAc Sniffing Library](https://gitlab.com/shockrahwow/wifi-sens)

A small wrapper library to easily get the [ESP8266](https://www.espressif.com/en/products/hardware/esp8266ex/overview) usable in promiscuous mode, allowing for really easy passive wifi sniffing with an arduino.

* Languages used: C/C++, Bash

## [Ripsy - ELF Parser](https://gitlab.com/shockrahwow/hexd)

A "multi"-tool which to learn how to go about making useful dissassembly and reverse engineering tools for linux binaries.

* _Languages used_: C, Bash, Make

## [JankOS - Operating System made from Scratch](https://gitlab.com/shockrahwow/jankos)

A toy operating system which I leverage to educate myself about low levvel operating system operations.

* Languages used: C, x86Assembly, Bash, Make
